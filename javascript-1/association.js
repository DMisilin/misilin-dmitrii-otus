/**
 * Max association function
 * @param {array<array>} data Data of history users
 * @return {array<string>} result Max association list
 * */
function maxItemAssociation(data) {
    if (!data || data.length === 0) {
        return [];
    }

    let accum = [];

    for(let i = 0; i < data.length; i++) {
        let tempAssociation = [];

        for(let j = 0; j < data.length; j++) {
            if (i !== j) {
                isHaveGeneralElement(data[i], data[j]) && tempAssociation.push(...data[j]);
            }
        }

        tempAssociation.push(...data[i]);
        accum.push(removeDuplicateAndSort(tempAssociation));
    }

    const result = sortAndReturnFirstElement(accum);
    console.log('Result: ', result);

    return result;
}

/**
 * Common function. Check common element in two arrays
 * @param {array<string>} list1 List 1
 * @param {array<string>} list2 List 2
 * @return {boolean} true if common has
 * */
function isHaveGeneralElement(list1, list2) {
    return list1.filter(elm => list2.indexOf(elm) >= 0).length > 0;
}

/**
 * Common function. Remove duplicates and sort result
 * @param {array<string>} list List
 * @return {array<string>} sorted list
 * */
function removeDuplicateAndSort(list) {
    const temp = [...new Set(list)];

    return temp.sort();
}

/**
 * Common function. Sort and return first element
 * @param {array<array>} list List
 * @return {array<string>} result
 * */
function sortAndReturnFirstElement(list) {
    const sortedByLength = list.sort((listA, listB) => listA.length > listB.length);

    const sortedByAlphabet = sortedByLength.sort((listA, listB) => {
        if(listA.length === listB.length) {
            return listA.join('') > listB.join('') ? 1 : -1;
        }

        return 0;
    });

    return sortedByAlphabet[0];
}
