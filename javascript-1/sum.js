/**
 * Sum function
 * @param {number} data Data
 * @return {number} sum
 * */
const sum = (data) => {
    let result = data;

    return function common(value) {
        if(value) {
            result += value;
            return common;
        }

        console.log(`Result: '${result}'`);

        return result;
    }
}
