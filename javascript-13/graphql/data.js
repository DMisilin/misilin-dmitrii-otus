module.exports = {
    orders: [
        {
            orderId: 11,
            userId: 1,
            amount: 300,
            goods: [
                {title: 'fruit', count: 3},
            ]
        },
        {
            orderId: 12,
            userId: 1,
            amount: 500,
            goods: [
                {title: 'fruit', count: 6},
            ]
        },
        {
            orderId: 13,
            userId: 2,
            amount: 3600,
            goods: [
                {title: 'book', count: 1},
            ]
        },
        {
            orderId: 14,
            userId: 2,
            amount: 300,
            goods: [
                {title: 'mouse', count: 11},
            ]
        },
        {
            orderId: 15,
            userId: 3,
            amount: 800,
            goods: [
                {title: 'book', count: 2},
                {title: 'alcohol', count: 1},
            ]
        },
    ],
    goods: [
        {
            title: 'fruit',
            cost: 33,
            type: 'type1',
            balance: 20
        },
        {
            title: 'mouse',
            cost: 500,
            type: 'type2',
            balance: 2
        },
        {
            title: 'book',
            cost: 100,
            type: 'type3',
            balance: 13
        },
        {
            title: 'alcohol',
            cost: 1300,
            type: 'type4',
            balance: 5
        }
    ],
    users: [
        {
            userId: 1,
            login: 'Use1'
        },
        {
            userId: 2,
            login: 'Use2'
        },
        {
            userId: 3,
            login: 'Use3'
        },
    ]
};
