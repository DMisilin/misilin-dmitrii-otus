## MUTATION

Программный интерфейс приложения

### TOC

- [MUTATION](#mutation)
    - [TOC](#toc)
    - [createUser](#createuser)
        - [Example](#example)
    - [updateGoodsBalance](#updategoodsbalance)
        - [Example](#example)

##### createUser

Создание пользователя

###### Example

```
mutation {
  createUser (data: {userId: 2222, login: "login"})
}
```

#### updateGoodsBalance

Изменение остатка товара на складе

###### Example

```
mutation {
  updateGoodsBalance (title: "fruit", count: 1)
}
```
