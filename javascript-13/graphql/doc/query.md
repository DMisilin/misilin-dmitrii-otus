## QUERY

Программный интерфейс приложения

### TOC

- [QUERY](#query)
    - [TOC](#toc)
    - [data](#data)
        - [Example](#example)
    - [order](#wspiorderpe)
        - [Example](#example)
    - [userOrder](#userorder)
      - [Example](#example)    
    - [users](#users)
      - [Example](#example)
    - [goods](#goods)
      - [Example](#example)

##### data

Запрос дял проверки жизнеспособности сервера

###### Example

```
query {
  data
}
```

#### order

Получение информации о заказе по `id`

###### Example

```
query {
  order (id: 12) {
    orderId
    amount
    goods {
      title
      count
    }
  }
}
```

#### userOrder

Получение информации о заказах пользователя по его `userId`

###### Example

```
query {
  userOrder (userId: 2) {
    orderId
    userId
    amount
    goods {
      title
      count
    }
  }
}
```

#### users

Получение списка пользователей

###### Example

```
query {
  users {
    userId
    login
  }
}
```

#### goods

Получение списка товаров

###### Example

```
query {
  goods {
    title
    cost
    type
    balance
  }
}
```
