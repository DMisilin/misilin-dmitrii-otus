const express = require('express');
const {graphqlHTTP} = require('express-graphql');
const {GraphQLSchema} = require('graphql');
const app = express();

const query = require('./query.js');
const mutation = require('./mutation.js');

const PORT = 4000;

const schema = new GraphQLSchema({
    query,
    mutation,
});

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true,
}));

app.get('/', (req, res) => {
    res.send('It is work');
});

app.listen(PORT, () => console.log(`Server was started on port ${PORT}`));
