const {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString, GraphQLInt,
} = require('graphql');
const inputUser = require('./types/input/user.js');
const data = require('./data.js');

module.exports = new GraphQLObjectType({
    name: 'mutation',
    fields: {
        createUser: {
            type: new GraphQLNonNull(GraphQLString),
            args: {
                data: {type: inputUser}
            },
            resolve: (_, {data: {userId, login}}) => {
                data.users.push({userId, login});
                console.log('Info user list: ', data.users);
                return `User with id: ${userId} was added`;
            }
        },
        updateGoodsBalance: {
            type: GraphQLString,
            args: {
                // по хорошему тут должен быть некий идентификтаор продукта, но тк это тест, то упор просто не механику
                title: {type: GraphQLString},
                count: {type: GraphQLInt}
            },
            resolve: (_, {title: updatedTitle, count}) => {
                for (let i = 0; i < data.goods.length; i++) {
                    const {title} = data.goods[i];

                    if (title === updatedTitle) {
                        if (data.goods[i].balance < count) throw new Error(`Balance ${data.goods[i].balance} less than count`);

                        data.goods[i].balance -= count;
                        return `Balance of ${updatedTitle} = ${data.goods[i].balance}`;
                    }
                }

                throw new Error(`Goods by title ${updatedTitle} not found`);
            }
        }
    }
})
