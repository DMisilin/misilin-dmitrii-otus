const {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
} = require('graphql');
const data = require('./data.js');
const order = require('./types/main/order.js');
const user = require('./types/main/user.js');
const goods = require('./types/main/goods.js');

module.exports = new GraphQLObjectType({
    name: 'query',
    fields: {
        data: {
            description: 'Work test',
            type: new GraphQLNonNull(GraphQLString),
            resolve: () => 'Hello from graphql'
        },
        order: {
            type: new GraphQLNonNull(order),
            args: {
                id: {type: GraphQLInt}
            },
            resolve: (_, {id}) => {
                const filtered = data.orders.filter(({orderId}) => orderId === id);
                return filtered[0] || {};
            }
        },
        userOrder: {
            type: new GraphQLList(order),
            args: {
                userId: {type: GraphQLInt}
            },
            resolve: (_, {userId}) => {
                return data.orders.filter(({userId: id}) => id === userId);;
            }
        },
        users: {
            type: new GraphQLList(user),
            resolve: () => data.users
        },
        goods: {
            type: new GraphQLList(goods),
            resolve: () => data.goods
        }
    }
})