const {
    GraphQLInputObjectType,
    GraphQLInt,
    GraphQLString,
} = require("graphql");

module.exports = new GraphQLInputObjectType({
    name: 'inputUser',
    description: 'input-user',
    fields: {
        userId: {type: GraphQLInt},
        login: {type: GraphQLString},
    }
})