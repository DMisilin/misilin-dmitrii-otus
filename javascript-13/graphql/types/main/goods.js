const {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
} = require('graphql');

module.exports = new GraphQLObjectType({
    name: 'goods',
    fields: {
        title: {type: GraphQLString},
        cost: {type: GraphQLInt},
        type: {type: GraphQLString},
        balance: {type: GraphQLInt}
    }
})