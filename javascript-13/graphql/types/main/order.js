const {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLList, GraphQLString,
} = require('graphql');

const goods = new GraphQLObjectType({
    name: 'orderGoods',
    fields: {
        title: {type: GraphQLString},
        count: {type: GraphQLInt},
    }
});

module.exports = new GraphQLObjectType({
    name: 'oder',
    fields: {
        orderId: {type: GraphQLInt},
        userId: {type: GraphQLInt},
        amount: {type: GraphQLInt},
        goods: {type: new GraphQLList(goods)}
    }
})
