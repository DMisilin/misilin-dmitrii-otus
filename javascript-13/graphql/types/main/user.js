const {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
} = require('graphql');

module.exports = new GraphQLObjectType({
    name: 'user',
    fields: {
        userId: {type: GraphQLInt},
        login: {type: GraphQLString}
    }
});
