module.exports = {
    mongo: {
        host: 'mongodb://localhost:27017',
        dbName: 'RSS',
        urlsCollection: 'URLs',
        documentCollection: 'document',
    },
    port: 4001
}