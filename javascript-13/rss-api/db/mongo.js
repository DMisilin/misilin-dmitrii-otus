const {MongoClient, ObjectId} = require('mongodb');
const {mongo: {host, dbName, urlsCollection, documentCollection}} = require('../config.js');

class MongoDB {
    constructor() {
        this.client = new MongoClient(host);
        this.connect = null;
    }

    async initConnect() {
        await this.client.connect();
        console.log('Connect to MongoDB was created');

        this.connect = this.client.db(dbName);
    }

    async getConnection() {
        !this.connect && await this.initConnect();

        return this.connect;
    }

    getURLs() {
        return this.connect.collection(urlsCollection).find().toArray();
    }

    getDocuments() {
        return this.connect.collection(documentCollection).find().toArray();
    }

    async addURL(data) {
        const url = await this.connect.collection(urlsCollection).findOne(data);

        if (url) {
            throw new Error('URL already exist');
        }

        await this.connect.collection(urlsCollection).insertOne(data);
    }

    async updateDocuments(documents) {
        if (documents.length) {
            for (let {title, author, ...data} of documents) {
                const filter = {title, author};
                const updateDoc = {
                    $set: {data}
                }

                await this.connect.collection(documentCollection).updateOne(filter, updateDoc, {upsert: true});
            }
        }
    }

    removeDocuments({ids}) {
        const objectIds = ids.map(id => ObjectId(id));
        return this.connect.collection(documentCollection).deleteMany({_id: {$in: objectIds}});
    }
}

module.exports = MongoDB;
