const express = require('express');
const {port} = require('./config.js');
const router = require('./routers/index.js');

const app = express();

app.use(express.json());
app.use('/', router);

app.listen(port, async () => console.log(`Server was started on port ${port}`));
