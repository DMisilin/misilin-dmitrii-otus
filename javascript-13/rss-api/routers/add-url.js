const MongoDB = require('../db/mongo.js');
const db = new MongoDB();

async function addUrl(req, res) {
    try {
        await db.getConnection();

        const {data} = req.body;
        await db.addURL(data);

        res.status(201).send({data: 'url added'});
    } catch (e) {
        res.status(500).send({error: `Error! Message: '${e.message}'`});
    }
}

module.exports = addUrl;
