const MongoDB = require('../db/mongo.js');
const db = new MongoDB();

async function getDocs(req, res) {
    await db.getConnection();

    const data = await db.getDocuments();
    res.send(data);
}

module.exports = getDocs;
