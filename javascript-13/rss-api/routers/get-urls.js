const MongoDB = require('../db/mongo.js');
const db = new MongoDB();

async function getUrls(req, res) {
    await db.getConnection();

    const data = await db.getURLs();
    res.send(data.map(({_id, ...data}) => data));
}

module.exports = getUrls;
