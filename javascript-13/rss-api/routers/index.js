const {Router} = require('express');

const getUrls = require('./get-urls.js');
const getDocs = require('./get-docs.js');
const updateDocs = require('./update-docs.js');
const remoteDocs = require('./remote-docs.js');
const addUrl = require('./add-url.js');

const router = Router();

router.get('/', updateDocs);
router.get('/urls', getUrls);
router.get('/docs', getDocs);
router.post('/remote_docs', remoteDocs);
router.post('/add_url', addUrl);

module.exports = router;
