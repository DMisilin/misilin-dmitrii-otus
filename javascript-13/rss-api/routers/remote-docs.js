const MongoDB = require('../db/mongo.js');
const db = new MongoDB();

// метод для очистки документов по id
async function remoteDocs(req, res) {
    try {
        await db.getConnection();

        const {data} = req.body;
        const result = await db.removeDocuments(data);

        res.status(201).send(result);
    } catch(e) {
        res.status(500).send({error: `Error! Message: '${e.message}'`});
    }
}

module.exports = remoteDocs;
