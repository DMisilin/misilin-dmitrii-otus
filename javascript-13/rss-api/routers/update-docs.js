const Parser = require("rss-parser");
const MongoDB = require('../db/mongo.js');

const db = new MongoDB();
const parser = new Parser();

async function updateDocument (req, res) {
    await db.getConnection();

    const urls = await db.getURLs();
    let documents = [];

    for (let {url} of urls) {
        const {items = []} = await parser.parseURL(url);
        documents = [...documents, ...items];
    }

    await db.updateDocuments(documents);

    res.send('Documents was updated');
}

module.exports = updateDocument;
