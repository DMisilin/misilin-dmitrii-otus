const getDocs = require('../routers/get-docs.js');

describe('Get Docs', function () {

    test('Type of responds to /docs', async () => {
        const req = {};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            }
        };

        await getDocs(req, res);

        expect(typeof res.text).toBe('object');
    });

});
