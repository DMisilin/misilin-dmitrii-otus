const getUrls = require('../routers/get-urls.js');

describe('Get URLs', function () {

    test('responds to /urls', async () => {
        const req = {};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            }
        };

        await getUrls(req, res);

        expect(res.text).toEqual([{url: 'https://www.reddit.com/.rss'}]);
    });

    test('length of responds to /urls', async () => {
        const req = {};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            }
        };

        await getUrls(req, res);

        expect(res.text.length).toBe(1);
    });

});
