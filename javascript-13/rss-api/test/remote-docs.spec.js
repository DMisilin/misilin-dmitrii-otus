const remoteDocsSpec = require('../routers/remote-docs.js');

describe('Remote Docs', function () {

    test('Type of responds to /remote_docs', async () => {
        const req = {body: {data: {ids: []}}};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            },
            status: function (input) {
                this.status = input;
                return this;
            }
        };

        await remoteDocsSpec(req, res);

        expect(typeof res.text).toBe('object');
    });

    test('Responds to /remote_docs', async () => {
        const req = {body: {data: {ids: []}}};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            },
            status: function (input) {
                this.status = input;
                return this;
            }
        };

        await remoteDocsSpec(req, res);

        expect(res.text.acknowledged).toBe(true);
        expect(res.text.deletedCount).toBe(0);
    });

});
