const updateDoc = require('../routers/update-docs.js');

describe('Update Docs', function () {

    test('Type of responds to /', async () => {
        const req = {};
        const res = {
            text: '',
            send: function (input) {
                return this.text = input
            }
        };

        await updateDoc(req, res);

        expect(res.text).toBe('Documents was updated');
    });

});
