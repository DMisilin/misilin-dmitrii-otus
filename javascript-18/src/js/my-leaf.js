class MyLeaf extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        const items = this.getAttribute('items');
        const indent = this.getAttribute('indent');
        const treeItems = JSON.parse(items).items;
        const shadow = this.attachShadow({mode: 'open'});

        if (treeItems && treeItems.length) {

            for (let item of treeItems) {
                const leaf = document.createElement('my-leaf');
                leaf.setAttribute('items', JSON.stringify({"items": item.items}));
                leaf.setAttribute('indent', '  ' + indent);
                leaf.innerText = `Leaf ${item.id}`;
                console.log(`${indent}- Leaf ${item.id} `);

                shadow.appendChild(leaf);
            }
        }
    }

    disconnectedCallback() {}

    static get observedAttributes() {
        return [];
    }

    attributeChangedCallback(name, oldValue, newValue) {}

    adoptedCallback() {}
}

customElements.define("my-leaf", MyLeaf);
