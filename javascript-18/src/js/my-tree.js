class MyTree extends HTMLElement {
    constructor() {
        super();
    }

    async connectedCallback() {
        fetch("./tree.json")
            .then(res => res.json())
            .then(tree => {
                console.log('Json: ', tree);

                const shadow = this.attachShadow({mode: 'open'});
                console.log(`Tree:`);

                if (tree) {
                    const leaf = document.createElement('my-leaf');
                    leaf.setAttribute('items', JSON.stringify({"items": tree.items,}));
                    leaf.setAttribute('indent', '  ');
                    leaf.innerText = `Leaf ${tree.id}`;
                    console.log(` - Leaf ${tree.id}`);

                    shadow.appendChild(leaf);
                }
            })
            .catch(err => console.error('Error load JSON: ', err.message));
    }

    disconnectedCallback() {}

    static get observedAttributes() {
        return [];
    }

    attributeChangedCallback(name, oldValue, newValue) {}

    adoptedCallback() {}
}

customElements.define("my-tree", MyTree);
