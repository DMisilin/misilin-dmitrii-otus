"use strict";
exports.__esModule = true;
var Ajv = require('ajv');
var schema = require('./config-schema.js');
var config = require('../../config/config.json');
var Log = require('../log/logger.js');
var Config = (function () {
    function Config() {
        this.config = null;
        this.validator = null;
        this.log = new Log();
    }
    Config.prototype.get = function () {
        if (!this.config) {
            this.validate();
            this.config = config;
        }
        return this.config;
    };
    Config.prototype.validate = function () {
        try {
            var ajv = new Ajv();
            this.validator = ajv.compile(schema);
        }
        catch (err) {
            this.log.error(err.message);
            throw new Error('Error create validator');
        }
        if (!this.validator(config)) {
            this.log.error(this.validator.errors);
            throw new Error('Config not valid');
        }
    };
    return Config;
}());
module.exports = Config;
//# sourceMappingURL=index.js.map