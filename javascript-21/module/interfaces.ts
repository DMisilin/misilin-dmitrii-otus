import {v4 as uuidV4} from "uuid";

export interface PrintParams {
    level: string,
    message: string,
    label: string,
    timestamp: string
}

export interface Logger {
    format: any,
    log: any
}

export interface LogInterface {
    format: any,
    log: any,
    info: (msg: string) => void
    error: (msg: string) => void
}

export interface ConfigInterface {
    config: Config,
    validator: any,
    log: LogInterface,
    content: Content
}

export interface PGInterface {
    getQueryResult: (queryName: string, params: any) => Promise<any>
    prepareQuery: (query: string, params: any) => string
}

export interface ModifyContentParams {
    hash: string,
    hashPrev: string,
    hashNext: string,
    text: string,
    first: boolean,
    last: boolean
}

export interface AddContentParams {
    texts: string[],
    bookId: number,
    last: boolean,
    currentHash: string,
    oldHashPref: string,
    oldHashNext: string
}

export interface AddContentOnCleanParams {
    text: string,
    bookId: number
}

export interface ShowSpendTimeParams {
    startTime: any,
    url: string
}

interface Config {
    config: ConfigInterface
    validator: any
    log: LogInterface
}

interface Content {
    maxSize: number
    minSize: number
}
