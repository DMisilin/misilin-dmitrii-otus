"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
exports.__esModule = true;
var _a = require('winston'), createLogger = _a.createLogger, format = _a.format, transports = _a.transports;
var combine = format.combine, timestamp = format.timestamp, label = format.label, printf = format.printf;
var version = require('../../package.json').version;
var myFormat = printf(function (printParams) {
    var level = printParams.level, message = printParams.message, label = printParams.label, timestamp = printParams.timestamp, args = __rest(printParams, ["level", "message", "label", "timestamp"]);
    var data = JSON.stringify(args).replace(/\\/g, '');
    return "".concat(timestamp, " [v").concat(label, "] ").concat(level, ": ").concat(message, ". data: ").concat(data);
});
var Log = (function () {
    function Log() {
        this.logger = createLogger({
            format: combine(label({ label: version }), timestamp(), myFormat),
            transports: [new transports.Console()]
        });
    }
    Log.prototype.info = function (msg) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.logger.log('info', msg, args);
    };
    Log.prototype.error = function (msg) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        this.logger.log('error', msg, args);
    };
    return Log;
}());
module.exports = Log;
//# sourceMappingURL=logger.js.map