const {createLogger, format, transports} = require('winston');
const {combine, timestamp, label, printf} = format;
const {version} = require('../../package.json');

import {PrintParams, Logger} from '../interfaces.js';

const myFormat = printf((printParams: PrintParams) => {
    const {level, message, label, timestamp, ...args} = printParams;
    const data = JSON.stringify(args).replace(/\\/g, '');
    return `${timestamp} [v${label}] ${level}: ${message}. data: ${data}`;
});

// @ts-ignore
class Log {
    logger: Logger
    /**
     * Log Constructor
     * */
    constructor() {
        this.logger = createLogger({
            format: combine(
                label({label: version}),
                timestamp(),
                myFormat
            ),
            transports: [new transports.Console()]
        });
    }

    /**
     * Method info log
     * @param {string} msg Text
     * @param {Array} args Params
     * */
    info(msg: string, ...args: any) {
        this.logger.log('info', msg, args);
    }

    /**
     * Method erro log
     * @param {string} msg Text
     * @param {Array} args Params
     * */
    error(msg: string, ...args: any) {
        this.logger.log('error', msg, args);
    }
}

module.exports = Log;
