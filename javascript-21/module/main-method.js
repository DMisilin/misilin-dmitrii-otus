"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var uuidV4 = require('uuid').v4;
var db = require('./postgres/pg.js');
var Config = require('./config');
var Log = require('./log/logger.js');
var MainMethod = (function () {
    function MainMethod() {
        this.db = db;
        this.config = new Config().get();
        this.log = new Log();
    }
    MainMethod.prototype.addContentOnClean = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var text, bookId, texts, result, addedHash, hashPrev, contentHash, hashNext, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        text = params.text, bookId = params.bookId;
                        texts = this.splitText(text);
                        result = [];
                        hashPrev = uuidV4();
                        contentHash = uuidV4();
                        hashNext = uuidV4();
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < texts.length)) return [3, 4];
                        return [4, this.db.getQueryResult('addContent', {
                                text: texts[i],
                                bookId: bookId,
                                hash: contentHash,
                                hashNext: hashNext,
                                hashPrev: hashPrev,
                                last: i === texts.length - 1,
                                first: i === 0
                            })];
                    case 2:
                        addedHash = (_a.sent())[0].hash;
                        result.push(addedHash);
                        hashPrev = contentHash;
                        contentHash = hashNext;
                        hashNext = uuidV4();
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3, 1];
                    case 4: return [2, { result: result }];
                }
            });
        });
    };
    MainMethod.prototype.addContent = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var texts, bookId, _a, last, currentHash, _b, oldHashPref, _c, oldHashNext, result, addedHash, hashPrev, contentHash, hashNext, i;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        texts = params.texts, bookId = params.bookId, _a = params.last, last = _a === void 0 ? false : _a, currentHash = params.currentHash, _b = params.oldHashPref, oldHashPref = _b === void 0 ? null : _b, _c = params.oldHashNext, oldHashNext = _c === void 0 ? null : _c;
                        result = [];
                        hashPrev = oldHashPref || uuidV4();
                        contentHash = currentHash || uuidV4();
                        hashNext = uuidV4();
                        i = 0;
                        _d.label = 1;
                    case 1:
                        if (!(i < texts.length)) return [3, 4];
                        return [4, this.db.getQueryResult('addContent', {
                                text: texts[i],
                                bookId: bookId,
                                hash: contentHash,
                                hashNext: hashNext,
                                hashPrev: hashPrev,
                                last: i === texts.length - 1 && last ? last : false,
                                first: false
                            })];
                    case 2:
                        addedHash = (_d.sent())[0].hash;
                        result.push(addedHash);
                        hashPrev = contentHash;
                        contentHash = hashNext;
                        hashNext = i === texts.length - 2 && oldHashNext ? oldHashNext : uuidV4();
                        _d.label = 3;
                    case 3:
                        i++;
                        return [3, 1];
                    case 4: return [2, { result: result, hashPrev: hashPrev, hashNext: hashNext }];
                }
            });
        });
    };
    MainMethod.prototype.modifyContent = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, hash, _b, hashPrev, _c, hashNext, text, _d, first, _e, last;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _a = params.hash, hash = _a === void 0 ? uuidV4() : _a, _b = params.hashPrev, hashPrev = _b === void 0 ? uuidV4() : _b, _c = params.hashNext, hashNext = _c === void 0 ? uuidV4() : _c, text = params.text, _d = params.first, first = _d === void 0 ? false : _d, _e = params.last, last = _e === void 0 ? false : _e;
                        return [4, this.db.getQueryResult('modifyContentWithText', {
                                text: text,
                                first: first,
                                last: last,
                                hashPrev: hashPrev,
                                hashNext: hashNext,
                                hash: hash
                            })];
                    case 1:
                        _f.sent();
                        return [2];
                }
            });
        });
    };
    MainMethod.prototype.splitText = function (text) {
        var result = [];
        var maxSize = this.config.content.maxSize;
        if (text.length <= maxSize)
            return [text];
        var tail = text;
        var head;
        do {
            head = tail.slice(0, maxSize / 2);
            tail = tail.slice(maxSize / 2);
            result.push(head);
        } while (tail.length > maxSize);
        result.push(tail);
        return result;
    };
    MainMethod.prototype.showSpendTime = function (params) {
        var _a = params.startTime, startTime = _a === void 0 ? new Date() : _a, url = params.url;
        var endTime = new Date();
        this.log.info("Spend time for '".concat(url, "' - ").concat((endTime - startTime) / 1000, "s"));
    };
    return MainMethod;
}());
module.exports = MainMethod;
//# sourceMappingURL=main-method.js.map