### README

# Description

Тестовое задание, для изучения и применения роутинга в React

# Run

Запуск приложение из папки `src` командой `npm run start`
Приложение запускается на `http://localhost:3000/`

# Features

Проложение состоит из двух страниц:
- страница с полем для поиска городов. Список городов хранится в локальном файле `city-list.js`, по нему же и происходит поиск
- страница с погодой. Погода запрашивается с открытого апи `api.openweathermap.org`. 
  Ответ содержит погоду на один день, по этому генерируется массив данных, длина которого равна настройке из конфига `weatherDayCount` 
  а каждый элемент содержит полученные значения температуры, ветра и состояния, уникально для каждого дня лишь название `${Наименование города}${День}`
