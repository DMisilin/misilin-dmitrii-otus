import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter, Route, Routes} from "react-router-dom";

import './index.css';
import './pages/home.css';
import './pages/town-weather.css';

import Home from './pages/home.js';
import TownWeather from "./pages/town-weather.js";

render(
    <BrowserRouter>
        <Routes>
            <Route path="/*" element={<Home/>} />
            <Route path="/weather/*" element={<TownWeather/>} />
        </Routes>
    </BrowserRouter>,
    document.getElementById("root")
);
