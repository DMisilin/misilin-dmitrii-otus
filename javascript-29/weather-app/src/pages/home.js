import {useState} from "react";
import cityList from '../city-list.js';
import {Link} from 'react-router-dom';

function Home() {
    const [cities, filterCities] = useState([]);

    const onChanged = (event) => {
        const {value} = event.target;

        if (!value || value.length < 3) {
            filterCities([]);
            return;
        }

        filterCities(cityList
            .filter((city) => {
            const s = city.match(new RegExp(`^${value}`, 'gi'));
            return s && s.length > 0;
            })
            .sort()
            .slice(0, 5));
    }

    const citiesList = () => {
        return cities.map((c, index) => {
            return (
                <div key={index}>
                    <Link className="LinkCity" to={`weather#${c}`}>{c}</Link>
                </div>);
        })
    }

    return (
        <div className="Home">
            <h1>[введите название города]</h1>
            <input className="Input" type="text" onChange={onChanged}/>
            <div>{citiesList()}</div>
        </div>
    );
}

export default Home;
