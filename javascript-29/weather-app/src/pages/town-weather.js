import {Link} from "react-router-dom";
import {useState} from "react";
import axios from "axios";
import config from '../config.js';

export default function TownWeather() {
    let [weather, setWeather] = useState({main: {temp: 273}, wind: {speed: 0}, weather: [{description: ''}]});
    const city = window.location.hash.slice(1);
    const url = `${config.url}?q=${city}&lang=ru&${config.key}`;
    let weatherData = {};
    const days = [];
    
    for (let i = 0; i < config.weatherDayCount; i++) {
        const timestamp = new Date().getTime();
        const dayArr = new Date(timestamp + i * 24 * 60 * 60 * 1000).toDateString().split(' ');
        days.push(`${dayArr[0]}-${dayArr[2]}`);
    }

    if (!weather.name) {
        axios.get(url)
            .then(({data}) => {
                createFakeWeatherData(data);
                setWeather(weatherData[days[0]]);
            })
            .catch(error => console.error(error));
    }

    // TODO от АПИ погоды возвращается прогноз за один день, в итоге для всех дней задаю один и тот же прогноз, но с разным name
    const createFakeWeatherData = (data) => {
        weatherData = days.reduce((acc, idx) => {
            acc[idx] = {...data, name: data.name + `#${idx}`}
            return acc;
        }, {});

        window.wd = weatherData;
    }

    const createWeatherDayList = () => {
        return days.map((elm, idx) => (
            <button className="ButtonDayWeather" key={idx} onClick={() => setWeather(window.wd[days[idx]])}>
                {days[idx]}
            </button>
        ));
    }

    return (
        <div className="TownWeather">
            <h1>Weather</h1>
            <div>{createWeatherDayList()}</div>
            <b>{`Город: ${weather.name}`}</b>
            <b>{`Температура: ${(weather.main.temp - 273).toFixed(2)}`}</b>
            <b>{`Ветер: ${weather.wind.speed} м/с`}</b>
            <b>{`Состояние: ${weather.weather[0].description}`}</b>
            <Link className="LinkBack" to="/">Back</Link>
        </div>
);
}
