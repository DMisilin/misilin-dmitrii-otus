import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslationComponent } from './translation/translation.component';
import { SettingsComponent } from './settings/settings.component'
import { DictionaryComponent } from './dictionary/dictionary.component';
import { RememberComponent } from './remember/remember.component';

const routes: Routes = [
  { path: 'dictionary', component: DictionaryComponent },
  { path: 'translation', component: TranslationComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'remember', component: RememberComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
