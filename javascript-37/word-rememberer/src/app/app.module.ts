import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslationComponent } from './translation/translation.component';
import { DictionaryComponent } from './dictionary/dictionary.component';
import { SettingsComponent } from './settings/settings.component';
import {FormsModule} from "@angular/forms";
import { RememberComponent } from './remember/remember.component';

@NgModule({
  declarations: [
    AppComponent,
    TranslationComponent,
    DictionaryComponent,
    SettingsComponent,
    RememberComponent
  ],
  imports: [
    FormsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
