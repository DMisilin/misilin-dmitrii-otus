import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Word } from '../structures/word';
import { DictionaryElement } from '../structures/dictionary-element';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css']
})
export class DictionaryComponent implements OnInit {

  name: string = '';
  data: Array<any> = [
    {ru: 'идти', en: 'go'},
    {ru: 'писать', en: 'write'}
  ]
  model = new Word('', '', 'en', 'ru');
  languages = ['en', 'ru'];

  //TODO сделать элемент под СЛОВО в списке словаря
  dictionaryList: Array<DictionaryElement> = [
    {ru: 'слово', en: 'word'},
    {ru: 'стол', en: 'table'},
    {ru: 'море', en: 'sea'},
  ];

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    // Просто начальное значение при старте
    this.updateDictionaryOnLocalStorage('dictionaryList');

    this.route.queryParams.subscribe(params => {
      this.name = params['name'];
    });
  }

  onSubmit() {
    const {word, translate, wordLanguage, translateLanguage} = this.model;
    const de = new DictionaryElement('', '');
    //TODO разобраться с plotOptions

    // @ts-ignore
    de[wordLanguage] = word;
    // @ts-ignore
    de[translateLanguage] = translate;

    this.dictionaryList.push(de);
    this.updateDictionaryOnLocalStorage('dictionaryList');
    this.clearInput();
  }

  updateDictionaryOnLocalStorage(key: string) {
    localStorage.setItem(key, JSON.stringify(this.dictionaryList));
  }

  clearInput(): void {
    this.model.translate = '';
    this.model.word = '';
  }

  //TODO можно добавить фильтрацию языков - удалять из списка тот, что уже выбран, на случай, если будет более 2-х

}
