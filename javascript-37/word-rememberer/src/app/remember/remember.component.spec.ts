import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RememberComponent } from './remember.component';

describe('RememberComponent', () => {
  let component: RememberComponent;
  let fixture: ComponentFixture<RememberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RememberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RememberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check start value question index', () => {
    expect(component.questionIndex).toBe(1);
  });

  it('check start value questions', () => {
    expect(component.questions.length).toBe(0);
  });

  it('check questions after start testing', () => {
    component.nextQuestion();
    expect(component.questionIndex).toBe(2);
  });

  it('testing flag after finish', () => {
    component.finishTesting();
    expect(component.testing).toBe(false);
  });

  it('testing flag after finish', () => {
    component.finishTesting();
    expect(component.showResult).toBe(true);
  });

  it('testing flag after finish', () => {
    component.startTesting();
    expect(component.showResult).toBe(false);
  });
});
