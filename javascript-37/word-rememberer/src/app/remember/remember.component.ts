import { Component, OnInit } from '@angular/core';
import { Word } from "../structures/word";
import { Settings } from '../structures/settings';

@Component({
  selector: 'app-remember',
  templateUrl: './remember.component.html',
  styleUrls: ['./remember.component.css']
})
export class RememberComponent implements OnInit {

  testing: boolean = false;
  showResult: boolean = false;
  currentWord: Word = new Word('', '', '', '');
  responseWord: Word = new Word('', '', '', '');
  questions: Array<Word> = [];
  questionIndex: number = 1;
  resultCountTrue: number = 0;
  settings: Settings = new Settings('ru', 'en', 10);
  response = new Word('', '', 'en', 'ru');

  constructor() {}

  ngOnInit(): void {}

  onSubmit() {}

  startTesting(): void {
    this.resetIndexes();
    this.showResult = false;
    this.questions = this.getQuestions();
    this.currentWord = this.questions[this.questionIndex - 1];
    this.testing = true;
    this.settings = JSON.parse(localStorage.getItem('settings') || '{}');
    console.log('Questions: ', this.questions);
  }

  finishTesting(): void {
    this.testing = false
    this.showResult = true;
  }

  nextQuestion(): void {
    if (this.response.translate === this.currentWord.translate) {
      this.resultCountTrue++;
    }

    this.response = new Word('', '', 'en', 'ru');
    this.questionIndex++;

    if (this.questionIndex > this.questions.length) {
      this.finishTesting();
      return;
    }

    this.currentWord = this.questions[this.questionIndex - 1];
    console.log('Current word: ', this.currentWord);
  }

  getQuestions(): Array<Word> {
    const {currentLang, translateLang, count} = JSON.parse(localStorage.getItem('settings') || '{}');
    const data = JSON.parse(localStorage.getItem('dictionaryList') || '{}');
    const result: Array<Word> = [];

    for (let i = 0; i < count; i++) {
      const idx = Math.floor(Math.random() * data.length);
      const word = data[idx][currentLang];
      const translate = data[idx][translateLang];

      result.push(new Word(word, translate, currentLang, translateLang));
    }

    return result;
  }

  resetIndexes(): void {
    this.questionIndex = 1;
    this.resultCountTrue = 0;
  }

}
