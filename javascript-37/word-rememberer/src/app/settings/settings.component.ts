import {Component, OnInit} from '@angular/core';
import {Settings} from '../structures/settings';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor() {}

  minQuestionCount: number = 3;
  maxQuestionCount: number = 10;
  model = new Settings('ru', 'en', 5);
  languages = ['en', 'ru'];

  ngOnInit(): void {
    if (!localStorage.getItem('settings')) {
      this.onSubmit();
    }
  }

  onSubmit(): void {
    localStorage.setItem('settings', JSON.stringify(this.model));
    console.log('Settings was saved');
  }

}
