export class Settings {
  constructor(
    public currentLang: string,
    public translateLang: string,
    public count: number
  ) {}
}
