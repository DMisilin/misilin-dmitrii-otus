export class Word{
  constructor(
    public word: string | null,
    public translate: string | null,
    public wordLanguage: string | null,
    public translateLanguage: string | null,
  ) {}
}
