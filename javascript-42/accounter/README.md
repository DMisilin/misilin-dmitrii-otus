# accounter

## Description

Приложение для развития устного счет.
Состоит из трех страниц:
- home - домашняя страница с историей тестов (отображается 10 последних результатов)
- settings - страница настроек, где можно задать время на тест и диапазон слагаемых чисел (сложность)
- game - страница для тестирования

### Start at localhost:8080
```
npm run serve
```

### Configuration

`min` - минимальное число диапазона умолчанию,
`max`- максимальное число диапазона умолчанию,
`timer` - время по умолчанию в секундах,
`timerMin` - минимальное значение таймера,
`timerMax` - максимальное значение таймера,

