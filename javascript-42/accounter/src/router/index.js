import { createWebHistory, createRouter } from "vue-router";

import Home from '../components/Home.vue'
import Settings from '../components/Settings.vue'
import Game from '../components/Game.vue'

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
  },
  {
    path: "/game",
    name: "Game",
    component: Game,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;