const getPath = (elm, elpParent = null, res = '') => {
    if (!elm) {
        throw new Error(`elm is empty`);
    }

    let partPath = '';
    let parent = elpParent || elm.parentNode;
    let element = elm;

    if (!parent || element.tagName.toLowerCase() === 'body') {
        return res;
    }

    if (parent.childNodes.length > 1) {
        const position = getChildNumber(parent.childNodes, element);
        partPath = `:nth-child(${position})`;
    }

    const result = res ? `${getTag(element)}${partPath} > ${res}` : `${getTag(element)}${partPath}`;

    return getPath(parent, parent.parentNode, result);
}

const getChildNumber = (childList, element) => {
    for (let i = 0; i < childList.length; i++) {
        if (element === childList[i]) {
            return ++i;
        }
    }
}

const getTag = (elm) => {
    return `${elm.tagName.toLowerCase()}`;
}

module.exports = {getPath};
