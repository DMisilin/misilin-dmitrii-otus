const {getPath} = require('./index.js');

describe('getPath2', () => {
        beforeAll(() => {
            document.body.innerHTML =
                '<div class="NameC" id="3456">' +
                    '<span>  ' +
                        '<div class="phone"></div>' +
                    '</span>' +
                    '<span>' +
                        '<div class="phone" id="er"></div>' +
                    '</span>' +
                        '<button id="button"></button>' +
                '</div>'
        });

        it('should return string', () => {
            const elm = document.getElementsByClassName(`NameC`);
            const result = getPath(elm);
            expect(typeof result).toBe('string');
        })

        it('should return working selector', () => {
            const elm = document.querySelector(`#er`);
            const selector = getPath(elm);
            const result = document.querySelector(selector);
            expect(result).toBe(elm);
        })

        it('should return unique selector', () => {
            // returning two elements
            const elm = document.getElementsByClassName(`phone`);
            const selector = getPath(elm[1]);
            const result = document.querySelector(selector);
            expect(result).toBe(elm[1]);
        })
    }
);
