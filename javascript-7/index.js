const fs = require("fs").promises;

const PATH = process.argv[2];
const result = {files: [], dirs: []};

(async () => {
    const treePath = async (path, res) => {
         if (!path || !res) {
            throw new Error(`'Path' and 'res' are required params`);
         }

        const list = await fs.readdir(path);

        for (const element of list) {
            const elementPath = `${path}/${element}`;

            if ((await fs.lstat(elementPath)).isDirectory()) {
                res.dirs.push(elementPath);

                await treePath(elementPath, res)
            }

            res.files.push(elementPath);
        }
    }

    await treePath(PATH, result);
    
    console.log('result:: ', result);
})();


