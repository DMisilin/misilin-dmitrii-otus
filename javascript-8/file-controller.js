const fsPromises = require('fs').promises;
const fs = require('fs');
const {createReadStream} = require('fs');

/** @class */
class FileController {
    /**
     * Class constructor
     * @param {string} path Path
     * @param {number} size Size
     * @constructor
     */
    constructor(path, size) {
        this.path = path;
        this.size = size;
        this.readStreams = [];
        this.dt = new Date().getTime();
    }

    /**
     * Create file and split
     * */
    async createFile() {
        await this.initialFileGenerate();

        let fileSize = 0;

        while (fileSize < this.size) {
            await fsPromises.appendFile(this.path, `${this.getRandomNumberString(this.size / 25)}`);
            const {size} = await fsPromises.stat(this.path);
            fileSize = size;
        }

        await this.readAndSplitFile();
    }

    /**
     * Reader
     * */
    async readAndSplitFile() {
        const readable = createReadStream(this.path, {highWaterMark: this.size / 100});
        let index = 0;

        readable.on('data', (chunk) => {
            const data = chunk.toString().split('').sort((a, b) => +a - +b).join().replace(/,/g, '');
            const path = `./tmp-files/${++index}.txt`;

            fs.writeFileSync(path, data, (err) => {
                console.error('writeFileSync error: ', err);
            });
            
            console.log('File index was generate:: ', index);

            // this.readStreams.push(createReadStream(path, {highWaterMark: 2}));
        });

        readable.on('end', () => {
            // ожидаем обработки последнего батча
            setTimeout(async () => {
                const files = await fsPromises.readdir('./tmp-files');

                for (let f of files) {
                    const content = await fsPromises.readFile(`./tmp-files/${f}`);
                    const contentStringify = content.toString();

                    let s1 = contentStringify[0];
                    let sIndex = 0;

                    for (let i = 0; i < contentStringify.length; i++) {
                        if (s1 === '9') {
                            await fsPromises.appendFile(`./tmp-files-1/${contentStringify[i]}.txt`, contentStringify.slice(sIndex, contentStringify.length - 1));
                            break;
                        }

                        if (s1 !== contentStringify[i]) {
                            await fsPromises.appendFile(`./tmp-files-1/${contentStringify[i - 1]}.txt`, contentStringify.slice(sIndex, i - 1));
                            s1 = contentStringify[i];
                            sIndex = i;
                        }
                    }
                }

                const directory = await fsPromises.readdir('./tmp-files-1');
                const directoryFiles = directory.sort((a, b) => +a.split('.')[0] - +b.split('.')[0]);

                for (let file of directoryFiles) {
                    const ctx = await fsPromises.readFile(`./tmp-files-1/${file}`);
                    await fsPromises.appendFile('./result.txt', ctx);
                }

                // TODO попробовать реализацию на readLine
                // const minimums = [];
                //
                // // собираем минимальные значения с каждого файла
                // for (const stream of this.readStreams) {
                //     minimums.push(stream.read(1).toString());
                // }
                //
                // let temp;
                // let min;
                //
                // do {
                //     // берем минимальное из минимальных
                //     const {min: whileMin, index} = this.getMinValueFromArray(minimums);
                //     min = whileMin;
                //
                //     if (min === null) {
                //         return;
                //     }
                //
                //     fs.appendFileSync('./result.txt', minimums[index], (err) => {
                //         console.error('writeFileSync error: ', err);
                //     });
                //
                //     // await events.once(this.readStreams[index], 'drain');
                //
                //     // обновляем минимальные значения
                //     temp = this.readStreams[index].read(1);
                //     minimums[index] = temp ? temp.toString() : null;
                // }
                // while (min >= 0)

                console.log('Finish! ', (new Date().getTime() - this.dt) / 1000);
            }, 1000);
        });
    }

    /**
     * Return random string
     * @param {number} stringLength Length of string
     * @return {string} result
     * */
    getRandomNumberString(stringLength) {
        let result = '';

        while(result.length <= stringLength) {
            const rnd = Math.random() * 1000;
            result += `${Math.round(rnd)}`;
        }

        return result.slice(0, stringLength);
    }

    /**
     * Return minimum value from array
     * @param {array<string>} arr Array
     * @return {object} result
     * */
    getMinValueFromArray(arr) {
        let min = null;
        let index = null;

        for (let i = 0; i < arr.length; i++) {
            if (min === null || min > +arr[i]) {
                min = arr[i] ? +arr[i] : arr[i];
                index = i;
            }
        }

        return {index, min};
    }

    async initialFileGenerate() {
        await fsPromises.rm(`./tmp-files`, {recursive: true, force: true})
        await fsPromises.mkdir(`./tmp-files`);

        await fsPromises.rm(`./tmp-files-1`, {recursive: true, force: true})
        await fsPromises.mkdir(`./tmp-files-1`);

        for (let i = 0; i < 10; i++) {
            await fsPromises.writeFile(`./tmp-files-1/${i}.txt`, '');
        }

        await fsPromises.writeFile(this.path, '');
        await fsPromises.writeFile('./result.txt', '');
    }
}

module.exports = FileController;
