const FileController = require('./file-controller.js');
const PATH = './tmp.txt';
const SIZE = 100 * 1e6;

(async () => {
    const fc = new FileController(PATH, SIZE);

    await fc.createFile();
})();
